package com.itau.dados;

public class App {
	public static void main(String[] args) {
		Dado dado = new Dado(20);
		Resultado[] resultados = Sorteador.sortear(dado, 3);
		
		Impressora.imprimir(resultados);
	}
}
