package com.itau.dados;

public class Resultado {
	public int[] resultados;
	
	public Resultado(int[] resultados) {
		this.resultados = resultados;
	}
	
	public int somar() {
		int total = 0;
		
		for(int resultado : resultados) {
			total += resultado;
		}
		
		return total;
	}
	
	public String toString() {
		String texto = new String();
		
		for(int resultado : resultados) {
			texto += resultado + ", ";
		}
		
		texto += somar();
		
		return texto;
	}
}
