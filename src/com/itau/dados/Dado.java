package com.itau.dados;

public class Dado {
	private int lados;
	
	public Dado(int lados) {
		this.lados = lados;
	}
	
	public int sortear() {
		Double valor = Math.ceil(Math.random() * lados);
		return valor.intValue();
	}
}
