package com.itau.dados;

public class Sorteador {
	
	public static Resultado sortear(Dado dado) {
		int[] valores = new int[3];
		
		for(int i = 0; i < 3; i++) {
			valores[i] = dado.sortear();
		}
		
		Resultado resultado = new Resultado(valores);
		
		return resultado;
	}
	
	public static Resultado[] sortear(Dado dado, int vezes) {
		Resultado[] resultados = new Resultado[vezes];
		
		for (int i = 0; i < resultados.length; i++) {
			resultados[i] = sortear(dado);
		}
		
		return resultados;
	}
}
